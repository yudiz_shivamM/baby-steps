### Day 1 – Getting Started with Angular

- Setting up environment
- Creating first Angular App
- Data Binding
- Interpolation
- Property binding
- Event binding
- Two-way data binding
- Using directives
- Using pipes
- Creating custom pipe

### Day 2 – Component Communication

- Nesting Components
- Communication with child component using @Input
- Communication with parent component using @Output
- Using Template Variables to interact with child components

### Day 3 – Directives and ViewEncapsulation

- Custom Attribute Directives
- @HostBinding
- @HostListener
- Credit Card Custom Directives
- Understanding Shadow DOM
- Styling Components
- Angular CSS Encapsulation

### Day 4 – Dependency Injection and Services

- Creating a service
- Various ‘provideIn’ options
- Singleton service
- Tree shaking service
- useValue
- useClass
- useFactory
- useExisting
- Global error handling
    
### Day 5 – Dependency Injection and Services

- Creating a service
- Various ‘provideIn’ options
- any
- Service in Lazy Loaded Modules

### Day 6 – Getting Started with Reactive Forms

- FormControl
- FormBuilder
- FormGroup
- FormArray
    
### Day 7 – Reactive Forms Continued

- Subscribing to value change
- Validation
- Custom Validation
- Dynamic Controls using FormArray

### Day 8 – Creating API in Node.js

- Configuring modules in NodeJS
- Configuring SQL Server
- Creating Node JS application for CRUD operations with SQL Server
- Creating REST API using Express JS
- Testing API using Postman

### Day 9 – Understanding RxJS

- Observable
- Observer
- Operators
- Subjects,
- BehaviorSubject,
- AsyncSubject,
- ReplaySubject

### Day 10 – Using API in Angular

- Reading data in Angular app
- Creating data
- Editing Data
- Deleting data

### Day 11 – Using Interceptors

- Create a normal Interceptor to change HTTP to HTTPS
- Caching of the response
- Adding request headers such as authorization or any custom header for- all outgoing requests.
- Handling HTTP response error
- Manipulating the URL
- Authentication for every ongoing request etc.

### Day 12  – Routings

- Creating Routing
- Routing Parameters
- Snapshot and observable approach
- Optional Route Parameters
- Query Parameters
- Data using Route Resolver
- Creating Child Routes
- Secondary Routes
- Route Guards
- Lazy Loading

### Day 13 – Change Detection and async pipe

- What is change detection
- Default strategy
- onPush strategy
- using observable
- async pipe
- detach
- reattach
- markForCheck
- detectChanges

### Day 14 – Dynamic loading Components

- Dynamic Component Loading
- Dynamic Module Loading
- Dynamic loading external library

### Day 15 – Angular Elements
- Convert Angular Component to Angular Elements
- Working with @Input and @Output in Angular Element
- Content projection with slot in Angular Element
- Packaging Angular Element